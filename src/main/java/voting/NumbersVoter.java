package voting;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by Wojtek on 08.12.2016.
 */
public class NumbersVoter implements Voter {
    public Vote vote(String[] text, int pos) {
        if(StringUtils.isNumeric(text[pos])){
            if(pos < text.length -1 && StringUtils.isNumeric(text[pos+1])){
                return Vote.YES;
            }
        }
        return Vote.ABSTAIN;
    }
}
