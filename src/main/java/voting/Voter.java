package voting;

/**
 * Created by Wojciech Trefon on 19.10.2016.
 */
public interface Voter {
    Vote vote (String[] text, int pos);
}
