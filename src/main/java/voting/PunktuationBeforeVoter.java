package voting;

/**
 * Created by Wojciech Trefon on 19.10.2016.
 */
public class PunktuationBeforeVoter implements Voter {
    public Vote vote(String[] text, int pos) {
        if (pos == 0 || pos == text.length -1){
            return Vote.ABSTAIN;
        }
        if (text[pos-1].contains(".") ||text[pos-1].contains(",") ) {
            return Vote.NO;
        } else {
            return Vote.ABSTAIN;
        }
    }
}
