package voting;


import java.util.Arrays;
import java.util.HashSet;

/**
 * Created by Wojtek on 08.12.2016.
 */
public class SkrotVoter implements Voter {

    private static String[] SKROT = {"jw", "cdn", "prof", "np", "ds", "itd", "etc", "itp", "płd", "płn", "str",
            "tzn", "tzw", "wsch", "zach", "zob", "lic", "dyr", "ang", "hab", "inż", "doc", "godz", "r", "tys"};

    private static HashSet<String> SKROT_SET = new HashSet<String>(Arrays.asList(SKROT));

    public Vote vote(String[] text, int pos) {
        if (SKROT_SET.contains(text[pos].toLowerCase())) {
            return Vote.YES;
        } else {
            return Vote.ABSTAIN;
        }
    }
}
