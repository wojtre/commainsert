package punctuationinsertion;

/**
 * Created by Wojciech Trefon on 19.10.2016.
 */
public interface PunctuationInserter {
    String insertPunctuation(String text);
}
