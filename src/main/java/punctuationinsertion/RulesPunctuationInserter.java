package punctuationinsertion;

import voting.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wojciech Trefon on 19.10.2016.
 */
public class RulesPunctuationInserter implements PunctuationInserter {

    // kolejnosci voterow wazna!!!
    private Voter[] commaBeforeVoters = {new PunktuationBeforeVoter(), new SpojnikVoter()};
    private Voter[] commaAfterVoters = {new KrzykVoter()};
    private Voter[] pointVoters = {new SkrotVoter()};
    private Voter[] pointInDateVoters = {new DateVoter()};
    private Voter[] dashVoters = {new NumbersVoter()};

    public String insertPunctuation(String text) {
        String[] splittedText = text.split(" ");
        List<String>[] punctuation = new List[splittedText.length + 1];
        for (int k = 0; k < punctuation.length; k++) {
            punctuation[k] = new ArrayList<String>();
        }
        for (int i = 0; i < splittedText.length; i++) {
            if (shouldBePunctuation(splittedText, i, commaBeforeVoters)) {
                punctuation[i].add(", ");
            }
            if (shouldBePunctuation(splittedText, i, commaAfterVoters)) {
                punctuation[i+1].add(", ");
            }
            if (shouldBePunctuation(splittedText, i, pointVoters)) {
                punctuation[i + 1].add(". ");

            }
            if (shouldBePunctuation(splittedText, i, pointInDateVoters)) {
                punctuation[i + 1].add(".");
            } else if (shouldBePunctuation(splittedText, i, dashVoters)) {
                punctuation[i + 1].add("-");
            }
        }

        return constructTextWithCommas(splittedText, punctuation);
    }

    private String constructTextWithCommas(String[] text, List<String>[] positions) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < text.length; i++) {
            if (!positions[i].isEmpty()) {
                for (String s : positions[i]) {
                    builder.append(s);
                }
                builder.append(text[i]);
            } else {
                builder.append(" " + text[i]);
            }

        }
        if (!positions[positions.length - 1].isEmpty()) {
            for (String s : positions[positions.length - 1]) {
                builder.append(s);
            }
        }
        return builder.toString();
    }

    private boolean shouldBePunctuation(String[] text, int pos, Voter[] voters) {
        for (Voter v : voters) {
            Vote vote = v.vote(text, pos);
            if (vote == Vote.YES) {
                return true;
            } else if (vote == Vote.NO) {
                return false;
            }
        }
        return false;
    }
}
