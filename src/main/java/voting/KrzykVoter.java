package voting;

import java.util.Arrays;
import java.util.HashSet;

/**
 * Created by Wojtek on 08.12.2016.
 */
public class KrzykVoter implements Voter {
    private static String[] KRZYK = {"och","ach", "halo", "hej", "ho", "oj"};

    private static HashSet<String> KRZYK_SET = new HashSet<String>(Arrays.asList(KRZYK));

    public Vote vote(String[] text, int pos) {
        if (KRZYK_SET.contains(text[pos].toLowerCase())) {
            return Vote.YES;
        } else {
            return Vote.ABSTAIN;
        }
    }
}
