package voting;

/**
 * Created by Wojciech Trefon on 19.10.2016.
 */
public enum Vote {
    YES, NO, ABSTAIN
}
