package voting;

import java.util.Arrays;
import java.util.HashSet;

/**
 * Created by Wojciech Trefon on 19.10.2016.
 */
public class SpojnikVoter implements Voter {
    private static String[] SPOJNIKI = {"aby", "acz", "aczkolwiek", "albowiem", "azali", "aż", "ażeby", "bo", "boć", "bowiem",
            "by", "byle", "byleby", "chociaż", "chociażby", "choć", "choćby", "chybaby", "co", "cokolwiek", "czy", "czyj",
            "dlaczego", "dlatego", "dokąd", "dokądkolwiek", "dopiero", "dopóki", "gdy", "gdyby", "gdyż", "gdzie",
            "gdziekolwiek", "ile", "ilekolwiek", "ilekroć", "im", "iż", "iżby", "jak", "jakby", "jaki", "jakikolwiek",
            "jakkolwiek", "jako", "jakoby", "jakżeby", "jeśli", "jeśliby", "jeżeli", "jeżeliby", "kędy", "kiedy",
            "kiedykolwiek", "kiedyż", "kim", "kogo", "komu", "kto", "ktokolwiek", "którędy", "który", "ledwie", "ledwo",
            "niech", "nim", "odkąd", "ponieważ", "póki", "skąd", "skądkolwiek", "skoro", "zaledwie", "zanim", "że", "żeby"};

   private static HashSet<String> SPOJNIKI_SET = new HashSet<String>(Arrays.asList(SPOJNIKI));

    public Vote vote(String[] text, int pos) {
        if(SPOJNIKI_SET.contains(text[pos])) {
            return Vote.YES;
        } else {
            return Vote.ABSTAIN;
        }
    }
}
