package voting;

import static org.apache.commons.lang3.StringUtils.isNumeric;

/**
 * Created by Wojtek on 08.12.2016.
 */
public class DateVoter implements Voter {


    public Vote vote(String[] text, int pos) {
        if (!isNumeric(text[pos])) {
            return Vote.NO;
        }

        if (isFirstElementOfDate(text, pos)) {
            return Vote.YES;
        }
        if (isLastElementOfDate(text, pos)) {
            return Vote.NO;
        }
        if (isMiddleElementOfDate(text, pos)) {
            return Vote.YES;
        }
        return Vote.ABSTAIN;


    }

    private boolean isMiddleElementOfDate(String[] text, int pos) {
        return pos <= text.length - 2 && pos >= 1 && isNumeric(text[pos + 1]) && isNumeric(text[pos - 1]);
    }

    private boolean isLastElementOfDate(String[] text, int pos) {
        return pos >= 2 && isNumeric(text[pos - 1]) && isNumeric(text[pos - 2]);
    }

    private boolean isFirstElementOfDate(String[] text, int pos) {
        return pos <= text.length - 3 && isNumeric(text[pos + 1]) && isNumeric(text[pos + 2]);
    }
}